import DependencyVersions._
import sbt._

object Dependencies {

  private val grpcDependencies = Seq(
    "io.grpc"                % "grpc-netty"            % grpcNettyVersion,
    "com.trueaccord.scalapb" %% "scalapb-runtime-grpc" % grpcRuntimeVersion
  )

  private val corsDependencies = Seq(
    "ch.megard" %% "akka-http-cors" % corsVersion
  )

  private val loggingDependencies = Seq(
    "ch.qos.logback"             % "logback-classic"  % logbackVersion,
    "ch.qos.logback"             % "logback-core"     % logbackVersion,
    "org.slf4j"                  % "jcl-over-slf4j"   % slf4jVersion,
    "org.slf4j"                  % "log4j-over-slf4j" % slf4jVersion,
    "org.slf4j"                  % "jul-to-slf4j"     % slf4jVersion,
    "com.typesafe.scala-logging" %% "scala-logging"   % scalaLoggingVersion
  )

  private val enumeratumDependencies = Seq(
    "com.beachape" %% "enumeratum" % enumeratumVersion
  )

  private val akkaHttp = Seq(
    "com.typesafe.akka" %% "akka-slf4j"       % akkaVersion,
    "com.typesafe.akka" %% "akka-stream"      % akkaVersion,
    "com.typesafe.akka" %% "akka-http"        % akkaHttpVersion,
    "de.heikoseeberger" %% "akka-http-json4s" % akkaHttpJson4sVersion,
    "org.json4s"        %% "json4s-native"    % json4sVersion
  )

  private val sangriaDependencies = Seq(
    "org.sangria-graphql" %% "sangria"               % sangriaVersion,
    "org.sangria-graphql" %% "sangria-json4s-native" % sangriaJson4sVersion,
    "org.sangria-graphql" %% "sangria-circe"         % sangriaCirceVersion
  )

  private val circeDependencies = Seq(
    "io.circe" %% "circe-core",
    "io.circe" %% "circe-generic",
    "io.circe" %% "circe-parser"
  ).map(_ % circeVersion)

  private val commonDependencies = Seq(
    "io.codeheroes" %% "codeheroes-commons" % codeheroesCommonsVersion
  )

  private val jwtDependencies = Seq(
    "com.pauldijou" %% "jwt-json4s-native" % jwtVersion
  )

  val all: Seq[ModuleID] =
    akkaHttp ++
      sangriaDependencies ++
      circeDependencies ++
      loggingDependencies ++
      commonDependencies ++
      jwtDependencies ++
      grpcDependencies ++
      corsDependencies ++
      enumeratumDependencies

  val additionalResolvers = Seq(
    Resolver.jcenterRepo,
    Resolver.mavenCentral,
    Resolver.bintrayRepo("codeheroes", "maven"),
    "Typesafe Repo" at "http://repo.typesafe.com/typesafe/releases/"
  )

  val globalExcludes = Seq(
    ExclusionRule("log4j"),
    ExclusionRule("log4j2"),
    ExclusionRule("commons-logging")
  )

}
