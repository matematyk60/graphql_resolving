import com.trueaccord.scalapb.compiler.Version

object DependencyVersions {
  val akkaVersion              = "2.5.17"
  val akkaHttpVersion          = "10.1.5"
  val logbackVersion           = "1.2.3"
  val slf4jVersion             = "1.7.25"
  val scalaLoggingVersion      = "3.9.0"
  val codeheroesCommonsVersion = "0.1"
  val sangriaVersion           = "1.4.2"
  val sangriaJson4sVersion     = "1.0.0"
  val jwtVersion               = "0.14.1"
  val json4sVersion            = "3.6.2"
  val akkaHttpJson4sVersion    = "1.22.0"
  val enumeratumVersion        = "1.5.13"
  val corsVersion              = "0.2.2"
  val circeVersion             = "0.9.3"
  val sangriaCirceVersion      = "1.2.1"

  val grpcNettyVersion: String   = Version.grpcJavaVersion
  val grpcRuntimeVersion: String = Version.scalapbVersion
}
