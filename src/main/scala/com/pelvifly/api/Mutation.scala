package com.pelvifly.api

import com.pelvifly.api.Mutation.{Example, ExampleA, ExampleB, Training}
import io.circe.generic.auto._
import org.json4s.JsonAST.{JString, JValue}
import org.json4s.native.JsonMethods._
import org.json4s.native.Serialization
import org.json4s.{CustomSerializer, DefaultFormats}
import sangria.macros.derive
import sangria.macros.derive.InputObjectTypeName
import sangria.marshalling.circe._
import sangria.marshalling.json4s.native.Json4sNativeResultMarshaller
import sangria.marshalling.{FromInput, ResultMarshaller}
import sangria.schema.{Argument, EnumType, EnumValue, Field, InputObjectType, ObjectType}

import scala.concurrent.{ExecutionContext, Future}

class Mutation()(implicit ec: ExecutionContext) {

  object XYX
      extends CustomSerializer[Example](_ =>
        ({
          case value: JString =>
            val x = "ala"
            value.s match {
              case "ExampleA" => ExampleA
              case "ExampleB" => ExampleB
            }
        }, {
          case ExampleA => JString("ExampleA")
          case ExampleB => JString("ExampleB")
        }))

  private implicit val formats = DefaultFormats ++ List(XYX)

  def w[T: Manifest] = new FromInput[T] {
    override val marshaller: ResultMarshaller = Json4sNativeResultMarshaller
    override def fromResult(node: marshaller.Node): T =
      Serialization.read[T](compact(render(node.asInstanceOf[JValue])))
  }

  implicit val ww: FromInput[Training] = w[Training]

  val ExampleArg  = Argument("exampleArg", Mutation.ExampleEnumType)
  val DateArg     = Argument("date", Mutation.DateInputType)
  val TrainingArg = Argument("bieda", Mutation.TrainingInputType)

  val MutationType = ObjectType[Unit, Unit](
    name = "Mutation",
    fields = sangria.schema.fields[Unit, Unit](
      Field(
        "Enum",
        sangria.schema.StringType,
        arguments = ExampleArg :: Nil,
        resolve = _ => {
          Future.successful("WWWWW")
        }
      ),
      Field(
        "NestedEnum",
        Mutation.ExampleEnumType,
        arguments = TrainingArg :: Nil,
        resolve = ctx => {
          val arg = ctx.arg(TrainingArg)
          Future.successful(arg.example)
        }
      )
    )
  )

}
object Mutation {

  implicit val ExampleEnumType = EnumType[Example](
    "Example",
    values = List(EnumValue(name = "ExampleA", value = ExampleA), EnumValue(name = "ExampleB", value = ExampleB))
  )

  val TrainingInputType = derive.deriveInputObjectType[Training](
    InputObjectTypeName("TrainingInputt")
  )

  implicit val DateInputType: InputObjectType[Date] = derive.deriveInputObjectType()

  final case class Training(example: Example)
  final case class Date(year: Int)

  sealed trait Example

  case object ExampleA extends Example
  case object ExampleB extends Example
}
