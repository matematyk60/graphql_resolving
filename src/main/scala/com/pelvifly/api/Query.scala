package com.pelvifly.api

import sangria.schema._

import scala.concurrent.{ExecutionContext, Future}

class Query()(implicit ec: ExecutionContext) {

  val QueryType = ObjectType[Unit, Unit](
    name = "Query",
    fields = fields[Unit, Unit](
      Field(
        "none",
        StringType,
        resolve = _ => Future.successful("String")
      ))
  )
}
