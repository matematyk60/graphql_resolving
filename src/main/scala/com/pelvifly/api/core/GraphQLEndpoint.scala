package com.pelvifly.api.core

import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import ch.megard.akka.http.cors.scaladsl.CorsDirectives.cors
import ch.megard.akka.http.cors.scaladsl.settings.CorsSettings
import com.pelvifly.api._
import de.heikoseeberger.akkahttpjson4s.Json4sSupport
import org.json4s.JsonAST.{JObject, JString}
import org.json4s.native.JsonMethods.parse
import org.json4s.native.Serialization
import org.json4s.{DefaultFormats, JValue, Serialization}
import sangria.execution._
import sangria.marshalling.json4s.native._
import sangria.parser.QueryParser
import sangria.schema.Schema

import scala.collection.immutable.Seq
import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Failure, Success}

class GraphQLEndpoint()(implicit mat: ActorMaterializer, ec: ExecutionContext) extends Json4sSupport {

  implicit val serialization: Serialization = Serialization
  implicit val formats: DefaultFormats      = DefaultFormats

  private val authenticationErrorKey = "special-authentication-error-key"

  private val allowedCorsMethods = Seq(GET, POST, PUT, DELETE, OPTIONS)
  private val corsSettings       = CorsSettings.defaultSettings.copy(allowedMethods = allowedCorsMethods)

  private val query    = new Query()
  private val mutation = new Mutation()

  private val schema = Schema(query.QueryType, Some(mutation.MutationType))
  private val handler = ExceptionHandler {
    case (_, _: IllegalMonitorStateException) => SingleHandledException(authenticationErrorKey)
  }

  private def graphQLEndpoint(json: JValue, bearerToken: Option[String]) = {
    val query     = (json \ "query").extract[String]
    val operation = (json \ "operationName").extractOpt[String]
    val vars      = (json \ "variables").extractOrElse[JObject](JObject())
    val context   = ()
    QueryParser.parse(query) match {
      case Success(queryAst) =>
        onComplete(Executor.execute(schema, queryAst, context, variables = vars, operationName = operation, exceptionHandler = handler)) {
          case Success(result) if (result \ "errors" \ "message").extract[List[String]].contains(authenticationErrorKey) =>
            complete(StatusCodes.Unauthorized)
          case Success(result)                    => complete(StatusCodes.OK                  -> result)
          case Failure(error: QueryAnalysisError) => complete(StatusCodes.BadRequest          -> error.resolveError)
          case Failure(error: ErrorWithResolver)  => complete(StatusCodes.InternalServerError -> error.resolveError)
          case Failure(other)                     => complete(StatusCodes.InternalServerError -> other.getMessage)

        }

      case Failure(error) ⇒ complete(StatusCodes.BadRequest -> JObject("error" -> JString(error.getMessage)))
    }
  }

  val route: Route =
    cors(corsSettings) {
      (post & path("graphql")) {
        optionalHeaderValueByName("Authorization") { bearerToken =>
          extractRequestEntity { entity ⇒
            onSuccess(entity.toStrict(10 seconds).map(_.data.utf8String).map(parse(_)))(json => graphQLEndpoint(json, bearerToken))
          }
        }
      }
    }
}
