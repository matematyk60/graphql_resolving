package com.pelvifly.api.core

import akka.actor.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.{server, Http}
import akka.stream.ActorMaterializer
import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

class HttpServer(bindHost: String, bindPort: Int, routes: server.Route*)(implicit system: ActorSystem, mat: ActorMaterializer, ec: ExecutionContext)
    extends StrictLogging {

  def start(): Unit = Http().bindAndHandle(routes.reduce(_ ~ _), bindHost, bindPort).onComplete {
    case Success(_)  => logger.info(s"HttpServer started at $bindHost:$bindPort")
    case Failure(ex) => logger.error(s"Failed to start HttpServer at $bindHost:$bindPort", ex)
  }

}
