package com.pelvifly

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.pelvifly.api.core.{GraphQLEndpoint, HttpServer}

import scala.concurrent.ExecutionContext

class Application()(implicit system: ActorSystem) {

  private implicit val materializer: ActorMaterializer    = ActorMaterializer()
  private implicit val executionContext: ExecutionContext = system.dispatcher

  private val endpoint = new GraphQLEndpoint()
  private val server   = new HttpServer("0.0.0.0", 8080, endpoint.route)

  def start(): Unit = server.start()

}
