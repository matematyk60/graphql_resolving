package com.pelvifly

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory

object Main extends App {

  private val config = ConfigFactory.load("default.conf")

  private implicit val system: ActorSystem = ActorSystem("PelviflyActorSystem", config)

  private val app = new Application()

  app.start()

}
