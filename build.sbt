name := "pel-admin-gateway"
scalaVersion := "2.12.7"
version := "0.7"

enablePlugins(SbtNativePackager)
enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)

val baseSettings = Seq(
  scalaVersion := "2.12.7",
  resolvers ++= Dependencies.additionalResolvers,
  excludeDependencies ++= Dependencies.globalExcludes,
  libraryDependencies ++= Dependencies.all,
  sources in (Compile, doc) := Seq.empty,
  publishArtifact in (Compile, packageDoc) := false,
  publishArtifact in packageDoc := false,
  parallelExecution in Test := true,
  scalafmtTestOnCompile in ThisBuild := true,
  scalafmtOnCompile in ThisBuild := false
)

val dockerSettings = Seq(
  dockerBaseImage := "java:openjdk-8",
  daemonUser in Docker := "root",
  dockerRepository := Some("docker.codeheroes.io"),
  dockerExposedPorts := Seq(8080)
)

val protoSettings = Seq(
  PB.targets in Compile := Seq(
    scalapb.gen(flatPackage = true) -> (sourceManaged in Compile).value
  )
)

lazy val `pel-admin-gateway` =
  project
    .in(file("."))
    .settings(baseSettings: _*)
    .settings(dockerSettings: _*)
    .settings(protoSettings: _*)

PB.protoSources in Compile := Seq(
  baseDirectory.value / "pel-proto-files" / "admin-service",
  baseDirectory.value / "pel-proto-files" / "client-service",
  baseDirectory.value / "pel-proto-files" / "training-service"
)
